const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');


const JQUERYFILENAME = 'jquery-3.5.1.min.js';
const jQueryPath = require.resolve(path.join(__dirname, JQUERYFILENAME));
const jQueryContent = fs.readFileSync(jQueryPath, 'utf8');


const getContentFromRelativePath = relPath => {
  const absPath = require.resolve(path.join(__dirname, relPath));
  return fs.readFileSync(absPath, 'utf8');
};


const injectJQuery = async page => {
  await page.evaluate(jQueryContent);
  return page;
};


const createBrowser = async config => {
  const {
    origin: url,
    overridedPermissions,
    headless,
  } = config;

  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: null,
  });

  const context = browser.defaultBrowserContext();
  context.overridePermissions(url, overridedPermissions);
  return browser;
};


const createNewPage = async (browser, url) => {
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: 'networkidle2' });
  return page;
};


const changePage = async (page, url) => {
  await page.goto(url, { waitUntil: 'networkidle2' });
  return page;
};


const closeBrowser = async browser => {
  await browser.close();
};


const login = async (page, selectors, credentials) => {
  console.log(`Attempt to login with selectors = ${JSON.stringify(selectors)}`);
  await page.click(selectors.email);
  await page.keyboard.type(credentials.username);
  await page.click(selectors.password);
  await page.keyboard.type(credentials.password);
  await page.click(selectors.button);
  await page.waitForNavigation();
  return page;
};


module.exports = {
  getContentFromRelativePath,
  injectJQuery,
  createBrowser,
  closeBrowser,
  createNewPage,
  changePage,
  login,
};
