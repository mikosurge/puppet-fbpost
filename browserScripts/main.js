// To debug on VsCode, change newline to '\n\n'
const NEWLINE = '\n\n';
// ==================== FACEBOOK NOTE SPECIFICS ====================
// The cover of the note
const NOTE_COVER = '._5i8q>._4-u2._39j6._4-u8>._4-u3._57u_._4-u4>._30q-';  //style="background-image: url(<url>);"
// The content of the note
const NOTE = '._4-u3>._4lmi';  // The whole wrapper: contains header and body
const NOTE_HEADER = `${NOTE}>._39k2`;  // Header: contains the note title, editor name, and the date created
const NOTE_TITLE = `${NOTE_HEADER}>._4lmk._5s6c`;  // The note title
const NOTE_META = `${NOTE_HEADER}>._2yud>._3uhg`;  // Meta: contains the note editor and date created
const NOTE_EDITOR = `${NOTE_META}>a._2yug`;  // The editor name
const NOTE_DATE = `${NOTE_META}>a._39g5`;  // The date created
const NOTE_BODY = `${NOTE}>._39k5._5s6c`;  // The whole body
// The social part: contains reactions and comments
const NOTE_SOCIAL = '._4-u3>._78tc>._78so>._68wo>._3vum';
const NOTE_REACTION = `${NOTE_SOCIAL}>._66lg>a._3dlf>span._3dlg>span>span._81hb`;
const NOTE_COMMENT_COUNT = `${NOTE_SOCIAL}>._4vn1>span._1whp._4vn2>a._3hg-._42ft`;
const NOTE_FIGURE_IMAGE = ['_h2z', '_297z', '_usd', 'img'];
// Note specific classes
const NOTE_FIGURE = '_4nuy';  // <figure>
const NOTE_CODE_BLOCK = '_19ik';  // <pre>
const SPAN_CODE = '_19ii';
const UNDERLINE = '_4yxr';
// ==================== FACEBOOK NOTE SPECIFICS ====================

// ==================== FACEBOOK POST SPECIFICS ====================
// Post selectors
const WHOLE_POST = '._5pcr.userContentWrapper';
const POST_MESSAGE = 'div[data-testid=post_message]';
// Container classes
const POST = ['_5pbx', 'userContent' ,'_3576'];
const CONTAINER = '_3w8y';
const HASHTAG = '_58cn';
const LINK = '_2u0z';
// Reaction and comment selectors
const REACTION = '._66lg>a._3dlf>span._3dlg>span._3dlh>span._81hb';
const REACTION_DETAILS = 'a._1n9l';
const COMMENT_COUNT = 'a._3hg-._42ft';
const TIMESTAMP_CONTAINER = '_5pcq';
const TIMESTAMP = '_5ptz';
/**
 * Image fetching
 * Following what the content offers, it's able to get the image only.
 * The image URL can be accessed without any access token, the order of prioritized URLs is as follows:
 * - a[rel=theater] => attr('data-ploi')
 * - a[rel=theater] => attr('data-plsi')  // The s960x960 one
 * - img._46-i.img => attr('data-src)
 */
const IMAGE_MULTIPLE_CONTAINER = ['_2a2q', '_65sr'];
const IMAGE_SINGLE_CONTAINER = ['_5cq3', '_1ktf'];
const IMAGE_MULTIPLE_WRAPPER = ['_5dec', '_xcx', '_487t'];  // <a rel="theater">
const IMAGE_SINGLE_WRAPPER = ['_4-eo', '_2t9n', '_50z9'];
const IMAGE_CROPPER = ['_46-h', '_4-ep', 'uiScaledImageContainer', '_517g'];  // The former is the main one, the smaller images in theater might have 'uiScaledImageContainer' class, sometimes we've got _517g
const IMAGE_INNER = ['_46-i', 'img', 'scaledImageFitHeight'];  // The smaller images in theater might have 'scaledImageFitHeight' class
const MORE_IMAGES = '_52db';
// ==================== FACEBOOK POST SPECIFICS ====================

// ==================== FACEBOOK SHARED ELEMENTS ====================
// Has-similar-tag element classes
const LINEAR = '_3dgx';
const HEADER1 = '_509y';
const HEADER2 = '_50a1';
const LIST = '_5a_q';
const ORDERED = '_509r';
const UNORDERED = '_5yj1';
const LIST_ITEM_UNORDERED = '_509q';
const LIST_ITEM_ORDERED = '_509s';
const BLOCKQUOTE = '_509u';
// Single line classes
const WRAPPED_LINE = '_2cuy';
const LEFT_ALIGNED = '_2vxa';
// Inner span classes
const ITALICS = '_4yxp';
const BOLD = '_4yxo';
const EMOJI = ['_47e3', '_5mfr'];  // [title="<emotion name>"]
const EMOJI_INNER = ['_7oe'];
// ==================== FACEBOOK SHARED ELEMENTS ====================

// ==================== ELEMENTS, TAGS, CLASSES, SELECTORS ====================
const ET = {
  POST: 'POST',
  CONTAINER: 'CONTAINER',
  LINEAR: 'LINEAR',
  PARAGRAPH: 'PARAGRAPH',
  HEADER1: 'HEADER1',
  HEADER2: 'HEADER2',
  BLOCKQUOTE: 'BLOCKQUOTE',
  LIST_ITEM_UNORDERED: 'LIST_ITEM_UNORDERED',
  LIST_ITEM_ORDERED: 'LIST_ITEM_ORDERED',
  LIST_ORDERED: 'LIST_ORDERED',
  LIST_UNORDERED: 'LIST_UNORDERED',
  SPAN_BOLD: 'SPAN_BOLD',
  SPAN_ITALICS: 'SPAN_ITALICS',
  SPAN_BOLD_ITALICS: 'SPAN_BOLD_ITALICS',
  SPAN_EMOJI: 'SPAN_EMOJI',
  SPAN_EMOJI_INNER: 'SPAN_EMOJI_INNER',
  SPAN_UNDERLINE: 'SPAN_UNDERLINE',
  SPAN_BOLD_UNDERLINE: 'SPAN_BOLD_UNDERLINE',
  LINK: 'LINK',
  LINK_HASHTAG: 'LINK_HASHTAG',
  LINEBREAK: 'LINEBREAK',
  IMAGE_MULTIPLE_CONTAINER: 'IMAGE_MULTIPLE_CONTAINER',
  IMAGE_MULTIPLE_WRAPPER: 'IMAGE_MULTIPLE_WRAPPER',
  IMAGE_SINGLE_WRAPPER: 'IMAGE_SINGLE_WRAPPER',
  IMAGE_SINGLE_CONTAINER: 'IMAGE_SINGLE_CONTAINER',
  IMAGE_CROPPER: 'IMAGE_CROPPER',
  IMAGE_INNER: 'IMAGE_INNER',
  MORE_IMAGES: 'MORE_IMAGES',
  TIMESTAMP_CONTAINER: 'TIMESTAMP_CONTAINER',
  TIMESTAMP: 'TIMESTAMP',
  EDITOR: 'EDITOR',
  WHOLE_POST: 'WHOLE_POST',
  REACTION: 'REACTION',
  REACTION_DETAILS: 'REACTION_DETAILS',
  COMMENT_COUNT: 'COMMENT_COUNT',
  NOTE: 'NOTE',  // Note
  NOTE_COVER: 'NOTE_COVER',
  NOTE_TITLE: 'NOTE_TITLE',
  NOTE_EDITOR: 'NOTE_EDITOR',
  NOTE_TIMESTAMP: 'NOTE_TIMESTAMP',
  NOTE_REACTION: 'NOTE_REACTION',
  NOTE_COMMENT_COUNT: 'NOTE_COMMENT_COUNT',
  NOTE_BODY: 'NOTE_BODY',
  NOTE_FIGURE_IMAGE: 'NOTE_FIGURE_IMAGE',
  FIGURE: 'FIGURE',  // Note
  CODE_BLOCK: 'CODE_BLOCK',  // Note
  SPAN_CODE: 'SPAN_CODE',  // Note
};

const FB_TAG = {
  [ET.POST]: 'DIV',
  [ET.CONTAINER]: 'DIV',
  [ET.LINEAR]: 'DIV',
  [ET.PARAGRAPH]: 'P',
  [ET.HEADER1]: 'H2',
  [ET.HEADER2]: 'H3',
  [ET.BLOCKQUOTE]: 'BLOCKQUOTE',
  [ET.LIST_ITEM_UNORDERED]: 'LI',
  [ET.LIST_ITEM_ORDERED]: 'LI',
  [ET.LIST_ORDERED]: 'OL',
  [ET.LIST_UNORDERED]: 'UL',
  [ET.SPAN_BOLD]: 'SPAN',
  [ET.SPAN_ITALICS]: 'SPAN',
  [ET.SPAN_BOLD_ITALICS]: 'SPAN',
  [ET.SPAN_EMOJI]: 'SPAN',
  [ET.SPAN_EMOJI_INNER]: 'SPAN',
  [ET.SPAN_UNDERLINE]: 'SPAN',
  [ET.SPAN_BOLD_UNDERLINE]: 'SPAN',
  [ET.LINK]: 'A',
  [ET.LINK_HASHTAG]: 'A',
  [ET.LINEBREAK]: 'BR',
  [ET.IMAGE_MULTIPLE_CONTAINER]: 'DIV',
  [ET.IMAGE_SINGLE_CONTAINER]: 'DIV',
  [ET.IMAGE_MULTIPLE_WRAPPER]: 'A',
  [ET.IMAGE_SINGLE_WRAPPER]: 'A',
  [ET.IMAGE_CROPPER]: 'DIV',
  [ET.IMAGE_INNER]: 'IMG',
  [ET.MORE_IMAGES]: 'DIV',
  [ET.TIMESTAMP_CONTAINER]: 'A',
  [ET.TIMESTAMP]: 'ABBR',
  [ET.NOTE]: 'DIV',
  [ET.FIGURE]: 'FIGURE',
  [ET.CODE_BLOCK]: 'PRE',
  [ET.SPAN_CODE]: 'SPAN',
  [ET.NOTE_FIGURE_IMAGE]: 'IMG',
};

const FB_CLS = {
  [ET.POST]: POST,
  [ET.CONTAINER]: [CONTAINER],
  [ET.LINEAR]: [WRAPPED_LINE, LINEAR, LEFT_ALIGNED],
  [ET.PARAGRAPH]: [],
  [ET.HEADER1]: [WRAPPED_LINE, HEADER1, LEFT_ALIGNED],
  [ET.HEADER2]: [WRAPPED_LINE, HEADER2, LEFT_ALIGNED],
  [ET.BLOCKQUOTE]: [WRAPPED_LINE, BLOCKQUOTE, LEFT_ALIGNED],
  [ET.LIST_ITEM_UNORDERED]: [WRAPPED_LINE, LIST_ITEM_UNORDERED, LEFT_ALIGNED],
  [ET.LIST_ITEM_ORDERED]: [WRAPPED_LINE, LIST_ITEM_ORDERED, LEFT_ALIGNED],
  [ET.LIST_ORDERED]: [LIST, ORDERED],
  [ET.LIST_UNORDERED]: [LIST, UNORDERED],
  [ET.SPAN_BOLD]: [BOLD],
  [ET.SPAN_ITALICS]: [ITALICS],
  [ET.SPAN_BOLD_ITALICS]: [BOLD, ITALICS],
  [ET.SPAN_EMOJI]: EMOJI,
  [ET.SPAN_EMOJI_INNER]: EMOJI_INNER,
  [ET.SPAN_UNDERLINE]: [UNDERLINE],
  [ET.SPAN_BOLD_UNDERLINE]: [BOLD, UNDERLINE],
  [ET.LINK]: [LINK],
  [ET.LINK_HASHTAG]: [HASHTAG],
  [ET.LINEBREAK]: [],
  [ET.IMAGE_MULTIPLE_CONTAINER]: IMAGE_MULTIPLE_CONTAINER,
  [ET.IMAGE_SINGLE_CONTAINER]: IMAGE_SINGLE_CONTAINER,
  [ET.IMAGE_MULTIPLE_WRAPPER]: IMAGE_MULTIPLE_WRAPPER,
  [ET.IMAGE_SINGLE_WRAPPER]: IMAGE_SINGLE_WRAPPER,
  [ET.IMAGE_CROPPER]: IMAGE_CROPPER,
  [ET.IMAGE_INNER]: IMAGE_INNER,
  [ET.MORE_IMAGES]: [MORE_IMAGES],
  [ET.TIMESTAMP_CONTAINER]: [TIMESTAMP_CONTAINER],
  [ET.TIMESTAMP]: [TIMESTAMP],
  [ET.FIGURE]: [WRAPPED_LINE, NOTE_FIGURE, LEFT_ALIGNED],
  [ET.CODE_BLOCK]: [NOTE_CODE_BLOCK],
  [ET.SPAN_CODE]: [WRAPPED_LINE, SPAN_CODE, LEFT_ALIGNED],
  [ET.NOTE_FIGURE_IMAGE]: NOTE_FIGURE_IMAGE
};

const FB_SEL = {
  [ET.POST]: `${POST_MESSAGE}`,
  [ET.IMAGE_CONTAINER]: '.mtm',
  [ET.CONTAINER]: `.${CONTAINER}`,
  [ET.LINEAR]: `.${WRAPPED_LINE}.${LINEAR}.${LEFT_ALIGNED}`,
  [ET.PARAGRAPH]: `p`,
  [ET.HEADER1]: `h2.${WRAPPED_LINE}.${HEADER1}.${LEFT_ALIGNED}`,
  [ET.HEADER2]: `h3.${WRAPPED_LINE}.${HEADER2}.${LEFT_ALIGNED}`,
  [ET.BLOCKQUOTE]: `blockquote.${WRAPPED_LINE}.${BLOCKQUOTE}.${LEFT_ALIGNED}`,
  [ET.LIST_ITEM_UNORDERED]: `li.${WRAPPED_LINE}.${LIST_ITEM_UNORDERED}.${LEFT_ALIGNED}`,
  [ET.LIST_ITEM_ORDERED]: `li.${WRAPPED_LINE}.${LIST_ITEM_ORDERED}.${LEFT_ALIGNED}`,
  [ET.LIST_ORDERED]: `ol.${LIST}.${ORDERED}`,
  [ET.LIST_UNORDERED]: `ul.${LIST}.${UNORDERED}`,
  [ET.SPAN_BOLD]: `span.${BOLD}`,
  [ET.SPAN_ITALICS]: `span.${ITALICS}`,
  [ET.SPAN_BOLD_ITALICS]: `span.${BOLD}.${ITALICS}`,
  [ET.SPAN_EMOJI]: `span.${EMOJI.join('.')}`,
  [ET.SPAN_EMOJI_INNER]: `span.${EMOJI_INNER.join('.')}`,
  [ET.SPAN_UNDERLINE]: `span.${UNDERLINE}`,
  [ET.SPAN_BOLD_UNDERLINE]: `span.${BOLD}.${UNDERLINE}`,
  [ET.LINK]: ``,
  [ET.LINK_HASHTAG]: `a.${HASHTAG}`,
  [ET.LINEBREAK]: `br`,
  [ET.IMAGE_MULTIPLE_CONTAINER]: `.${IMAGE_MULTIPLE_CONTAINER.join('.')}`,
  [ET.IMAGE_SINGLE_CONTAINER]: `${IMAGE_SINGLE_CONTAINER.join('.')}`,
  [ET.IMAGE_MULTIPLE_WRAPPER]: `a.${IMAGE_MULTIPLE_WRAPPER.join('.')}[rel=theater]`,
  [ET.IMAGE_SINGLE_WRAPPER]: `a.${IMAGE_SINGLE_WRAPPER.join('.')}[rel=theater]`,
  [ET.IMAGE_CROPPER]: `.${IMAGE_CROPPER.join('.')}`,
  [ET.IMAGE_INNER]: `img.${IMAGE_INNER.join('.')}`,
  [ET.MORE_IMAGES]: `._52d9>._52da`,  // >._52db
  [ET.TIMESTAMP_CONTAINER]: `a.${TIMESTAMP_CONTAINER}`,  // >abbr._5ptz
  [ET.TIMESTAMP]: `abbr.${TIMESTAMP}`,
  [ET.EDITOR]: `.fwb.fcg[data-ft=\\{\\"tn\\"\\:\\"\\;\\"\\}]>a`,
  [ET.WHOLE_POST]: WHOLE_POST,
  [ET.REACTION]: REACTION,
  [ET.REACTION_DETAILS]: REACTION_DETAILS,
  [ET.COMMENT_COUNT]: COMMENT_COUNT,
  [ET.NOTE]: NOTE,
  [ET.NOTE_COVER]: NOTE_COVER,
  [ET.NOTE_TITLE]: NOTE_TITLE,
  [ET.NOTE_EDITOR]: NOTE_EDITOR,
  [ET.NOTE_TIMESTAMP]: NOTE_DATE,
  [ET.NOTE_REACTION]: NOTE_REACTION,
  [ET.NOTE_COMMENT_COUNT]: NOTE_COMMENT_COUNT,
  [ET.NOTE_BODY]: NOTE_BODY,
  [ET.FIGURE]: `figure.${NOTE_FIGURE}`,
  [ET.NOTE_FIGURE_IMAGE]: `img.${NOTE_FIGURE_IMAGE.join('.')}`,
  [ET.CODE_BLOCK]: `pre.${NOTE_CODE_BLOCK}`,
  [ET.SPAN_CODE]: `span.${WRAPPED_LINE}.${SPAN_CODE}.${LEFT_ALIGNED}`,
};

// Markdown types
const MT = {
  LINEAR: 'LINEAR',
  BOLD: 'BOLD',
  ITALICS: 'ITALICS',
  BOLD_ITALICS: 'BOLD_ITALICS',
  UNDERLINE: 'UNDERLINE',
  BOLD_UNDERLINE: 'BOLD_UNDERLINE',
  LIST_ITEM_UNORDERED: 'LIST_ITEM_UNORDERED',
  LIST_ITEM_ORDERED: 'LIST_ITEM_ORDERED',
  LIST_ORDERED: 'LIST_ORDERED',
  LIST_UNORDERED: 'LIST_UNORDERED',
  LINK: 'LINK',
  NUMBERED_LIST: 'NUMBERED_LIST',  // unused
  QUOTE: 'QUOTE',
  H1: 'H1',
  H2: 'H2',
  INLINE_CODE : 'INLINE_CODE',
  LINEBREAK: 'LINEBREAK',
  IMAGE_FULL: 'IMAGE_FULL',
  IMAGE: 'IMAGE',
  HIDDEN: 'HIDDEN',
  CODE: 'CODE',
};
// ==================== ELEMENTS, TAGS, CLASSES, SELECTORS ====================

// ==================== REGEXES ====================
// l.facebook.com Regex
const REGEX_L_FB = /https:\/\/l.facebook.com\/l.php\?u=(http[s]?%3A%2F%2F.+)%3F.*/;
const REGEX_GROUP_POST = /https:\/\/www.facebook.com\/groups\/(.*)\/permalink\/(\d*)\/*/;
const REGEX_NOTE = /https:\/\/www.facebook.com\/notes\/(.*)\/(.*)\/(.*)\//;
const REGEX_STYLE = /background-image: url\(\'([^\(\']*)\'\);/;
const REGEX_NOTE_STYLE = /background-image: url\(([^\(\']*)\);/;

const REGEX_IMAGE_URL = /\\(.{2}) {1}/g;
const REGEX_ORIGIN = /^(https:\/\/|http:\/\/)?([A-Za-z0-9.]+)(\/[^\/]*){1}$/;
const REGEX_MEDIUM = /^https:\/\/medium.com(\/[^\/]*){2}$/;
// ==================== REGEXES ====================

// ==================== MAPPERS ====================
const mapGroupToCategory = {
  'vietnamquora': 'quora',
  'redditvietnam': 'reddit',
  '1988191741413952': 'quora',
  '366378530426222': 'reddit',
  'qrvn': 'quora',
};

const mapNameToId = {
  'vietnamquora': 1988191741413952,
  'redditvietnam': 366378530426222,
  'qrvn': 1988191741413952,
};

const mapElementToMarkdown = {
  [ET.LINEAR]: MT.LINEAR,
  [ET.HEADER1]: MT.H1,
  [ET.HEADER2]: MT.H2,
  [ET.BLOCKQUOTE]: MT.QUOTE,
  [ET.LIST_ITEM_UNORDERED]: MT.LIST_ITEM_UNORDERED,
  [ET.LIST_ITEM_ORDERED]: MT.LIST_ITEM_ORDERED,
  [ET.LIST_ORDERED]: MT.LIST_ORDERED,
  [ET.LIST_UNORDERED]: MT.LIST_UNORDERED,
  [ET.SPAN_BOLD]: MT.BOLD,
  [ET.SPAN_BOLD_ITALICS]: MT.BOLD_ITALICS,
  [ET.SPAN_ITALICS]: MT.ITALICS,
  [ET.SPAN_EMOJI_INNER]: MT.HIDDEN,
  [ET.SPAN_UNDERLINE]: MT.UNDERLINE,
  [ET.SPAN_BOLD_UNDERLINE]: MT.BOLD_UNDERLINE,
  [ET.SPAN_CODE]: MT.CODE,
  [ET.LINK]: MT.LINK,
  [ET.LINK_HASHTAG]: MT.LINK,
  [ET.LINEBREAK]: MT.LINEBREAK,
  [ET.IMAGE_MULTIPLE_WRAPPER]: MT.IMAGE_FULL,
  [ET.IMAGE_SINGLE_WRAPPER]: MT.IMAGE_FULL,
  [ET.IMAGE_INNER]: MT.IMAGE,
  [ET.NOTE_FIGURE_IMAGE]: MT.IMAGE,
};

const getCategoryFromGroup = group => mapGroupToCategory[group];
const getIdFromName = name => mapNameToId[name];
const getMarkdownFromElement = element => mapElementToMarkdown[element];
// ==================== MAPPERS ====================

// ==================== FORMATTERS ====================
/**
 * @description Format the given content by adding characters according to the given type.
 * The bold and italics format has some variant, can be either ***, ___, __* or **_
 * @param {String} type - Indicate which actions the method is gonna perform
 * @param {String} content - The content to format
 * @param {String} sub - The substitution if any
 */
const format = (type, content = null, sub = null, _post = new Post()) => {
  if (!type) {
    return content;
  }

  if (type === MT.LINEBREAK) {
    return NEWLINE;
  }

  content = content.trim();
  if (!content && (type !== MT.IMAGE_FULL && type !== MT.IMAGE)) {
    return '';
  }


  switch(type) {
    case MT.LINEAR:
      /**
       * Adding a linebreak after a linear is tricky, as the editor may unintentionally
       * add his linebreak already.
       * A workaround should be to disable every standalone <br> in a linear div tag.
       */
      // return `${content}\n`;
      return `${content}`;
    case MT.BOLD:
      return `**${content}**`;
    case MT.ITALICS:
      return `*${content}*`;
    case MT.BOLD_ITALICS:
      return `***${content}***`;
    case MT.UNDERLINE:
      return `<underline>${content}</underline>`;
    case MT.BOLD_UNDERLINE:
      return `**<underline>${content}</underline>**`;
    case MT.LIST_ITEM_UNORDERED:
      return `- ${content}`;
    case MT.LIST_ITEM_ORDERED:
      return `${++_post.orderedIndex}. ${content}`;
    case MT.LIST_ORDERED:
      // orderedIndex = 0;
      // return `${content}${NEWLINE}`;
      return `${content}`;
    case MT.LIST_UNORDERED:
      // return `${content}${NEWLINE}`;
      return `${content}`;
    case MT.LINK:
      // Judge sub origin
      if (sub.startsWith('/')) {
        sub = `https://www.facebook.com${sub}`;
      } else if (sub.startsWith('https://www.facebook.com')) {
        sub = sub.split('?')[0];
      } else {
        let lfb = REGEX_L_FB.exec(sub);
        if (lfb) {
          sub = decodeURIComponent(lfb[1]);
        }
      }

      if (!_post.origin) {
        let pattern = REGEX_ORIGIN.exec(sub);
        if (pattern) {
          let endpoint = pattern[2];
          // Ignore medium users
          if (!pattern[3].includes('@')) {
            _post.origin = judgeOrigin(endpoint) ? sub : null;
          }
        }

        pattern = REGEX_MEDIUM.exec(sub);
        if (pattern) {
          _post.origin = sub;
        }
      }
      return `[${content}](${sub})`;
    case MT.NUMBERED_LIST:
      return `${sub || 1}. ${content}`;
    case MT.QUOTE:
      // return `> ${content}${NEWLINE}`;
      return `> ${content}`;
    case MT.H1:
      return `# ${content}`;
    case MT.H2:
      // return `## ${content.toUpperCase()}`;
      return `## ${content}`;
    case MT.INLINE_CODE:
      return `\`${content}\``;
    case MT.IMAGE_FULL:
      _post.images.push({
        full: sub.dataPloi,
      });
      return `${content}`;
      break;
    case MT.IMAGE:
      return `![${sub.alt}](${sub.dataSrc})`;
    case MT.HIDDEN:
      return '';
    case MT.CODE:
      debugger;
      return `\`\`\`\n${content}\n\`\`\``;
    default:
      return content;
  }
};
// ==================== FORMATTERS ====================

// ==================== UTILITIES ====================
const isSubsetOf = (smaller, bigger) => {
  if (!Array.isArray(smaller) || !Array.isArray(bigger)) {
    return false;
  }

  return smaller.every(single => bigger.includes(single));
};

const judgeElements = (tag, classes) => {
  if (tag === 'BR') {
    return ET.LINEBREAK;
  }

  // Get rit of wrapped line and left aligned styles
  let strippedClasses = classes.filter(
    clz => clz !== WRAPPED_LINE || clz !== LEFT_ALIGNED
  );


  // Only 'A' and 'P' tag are allowed to have no classes
  if ((!strippedClasses || !strippedClasses.length) && tag !== 'A' && tag !== 'P') {
    return null;
  }

  // Loop through tags and classes for find something that matches
  for (let type of Object.keys(ET)) {
    if (tag === FB_TAG[type] && isSubsetOf(strippedClasses, FB_CLS[type])) {
      return type;
    }
  }
};

const possibleSources = [
  'qr.ae',
  'quora.com',
  'redd.it',
  'reddit.com',
  '.medium.com',  // link.medium.com, elemental.medium.com, ...
  'bit.ly',
];

const judgeOrigin = url => {
  return possibleSources.find(source => url.includes(source));
};

const stripToRaw = text => {
  return text
    .trim()
    .replace(/\*/g, '\\*')
    .replace(/\>/g, '\\>');
};

const hasLineBreak = text => {
  // NEWLINE is defined to debug so we set a trap here
  if (!NEWLINE) {
    return false;
  }

  return text.trim().includes(NEWLINE);
};

function retrieve($node, addNewLine = true, _post = new Post()) {
  let nodeContents = [];
  let children = $node.contents();

  if (!children.length) {
    // Retrieve texts from the leaf node, and gently check if any links exist
    let rawContent = stripToRaw($node.text());
    if (rawContent.startsWith('#') && rawContent.length < 20) {
      _post.tags.push(rawContent);
    }

    let pattern = REGEX_ORIGIN.exec(rawContent);
    if (pattern) {
      let endpoint = pattern[2];
      // Ignore medium users
      if (!pattern[3].includes('@')) {
        _post.origin = _post.origin || (judgeOrigin(endpoint) ? rawContent : null);
      }
    }

    pattern = REGEX_MEDIUM.exec(rawContent);
    if (pattern) {
      _post.origin = _post.origin || rawContent;
    }

    return rawContent;
  }

  children.each(function(index) {
    let child = $(this);
    let classes = child.attr('class');
    classes = classes ? classes.split(' ') : [];
    let tagName = child.prop('tagName');
    let element = judgeElements(tagName, classes);

    // Just one time being inline will make addNewLine false to all children
    let retrieved = retrieve(child, addNewLine && !isInline(element), _post);

    let formatType = element ? getMarkdownFromElement(element) : null;

    let subAttributes = null;
    if ((isSpan(element) || isInline(element)) && hasLineBreak(retrieved)) {
      // Separately format elements between line feeds
      let shards = retrieved.split(NEWLINE);
      let formattedShards = shards.map(
        shard => format(formatType, shard, null, _post)
      );
      nodeContents.push(formattedShards.join(NEWLINE));
    } else {
      // Links and images need an addition attributes for 'href'
      if (isLink(element)) {
        subAttributes = child.attr('href');
      } else if (isImage(element)) {
        subAttributes = {
          dataSrc: child.attr('data-src') || child.attr('src'),
          alt: child.attr('alt'),
        };
      } else if (isImageContainer(element)) {
        subAttributes = {
          dataPloi: child.attr('data-ploi'),
        };
      } else if (element === ET.NOTE_FIGURE_IMAGE) {
        subAttributes = {
          dataSrc: child.attr('src'),
          alt: child.attr('alt') || 'Image',
        };
      }
      nodeContents.push(format(formatType, retrieved, subAttributes, _post));
    }

    if (isList(element)) {
      // TODO handle indexing
      _post.orderedIndex = 0;
    }
  });

  return nodeContents.join(addNewLine ? NEWLINE : ' ');
};
// ==================== UTILITIES ====================

// ==================== IDENTITY FUNCTIONS ====================
const isInline = elem => (
  elem === ET.LINEAR ||
  elem === ET.HEADER1 || 
  elem === ET.HEADER2 || 
  elem == ET.LIST_ITEM_UNORDERED || 
  elem == ET.LIST_ITEM_ORDERED || 
  elem == ET.BLOCKQUOTE || 
  elem === ET.PARAGRAPH
);

const isLink = elem => (
  elem === ET.LINK ||
  elem === ET.LINK_HASHTAG
);

const isSpan = elem => (
  elem === ET.SPAN_BOLD || 
  elem === ET.SPAN_ITALICS || 
  elem === ET.SPAN_EMOJI
);

const isImage = elem => (
  elem === ET.IMAGE_INNER
);

const isImageContainer = elem => (
  elem === ET.IMAGE_MULTIPLE_WRAPPER ||
  elem === ET.IMAGE_SINGLE_WRAPPER
);

const isList = elem => (
  elem === ET.LIST_ORDERED || 
  elem === ET.LIST_UNORDERED
);
// ==================== IDENTITY FUNCTIONS ====================

// ==================== FETCH FUNCTIONS ====================
const fetchEditor = $node => $node.text();
const fetchTimestamp = $node => {
  return {
    timestamp: +$node.attr('data-utime'),
    timestampUTC: $node.attr('title')
  };
};
const fetchMain = ($node, post) => {
  return retrieve($node, true, post);
};
const fetchReactions = $node => parseInt($node.text());
const fetchReactionDetails = $node => {
  let valueAndKey = $node.attr('aria-label').split(' ');
  return {
    [valueAndKey[1].toLowerCase()]: parseInt(valueAndKey[0]),
  };
};
const fetchCommentCount = $node => parseInt($node.text().split(' ')[0]);
const fetchRemainingImages = $node => +$node.text();
const fetchImages = ($node, post) => {
  retrieve($node, true, post);
  const images = post.images; 
  return {
    images: images.map(image => ({ dataSrc: image.full, alt: 'Image' })),
    imageContent: images.map(image => format(MT.IMAGE, '', { dataSrc: image.full, alt: 'Image' })).join('\n\n'),
  };
};

// Note specifics
const fetchNoteTimestamp = $node => {
  return {
    timestamp: +(new Date($node.text()).getTime()),
    timestampUTC: $node.text(),
  };
};
const fetchNoteCover = $node => {
  let style = $node.attr('style');
  let urlPattern = REGEX_NOTE_STYLE.exec(style);
  let dataSrc = '';
  let alt = 'Image';
  if (urlPattern && urlPattern[1]) {
    dataSrc = urlPattern[1];
  }

  return {
    dataSrc,
    alt
  };
};
const fetchNoteTitle = $node => {
  return $node.text();
};
// ==================== FETCH FUNCTIONS ====================

// ==================== CLASSES ====================
class Post {
  constructor() {
    this.reset();
  }

  reset = () => {
    this.origin = '';  // The original link of this facebook post
    this.translationOrigin = '';  // The link of this facebook post
    this.translator = '';  // The person who translate the original post
    this.groupId = '';  // The facebook group id
    this.postId = '';  // The facebook post id
    this.format = 'md';
    this.category = '';  // 'reddit', 'quora', 'medium' or something similar
    this.collection = '';  // 'Future Attendance', 'Cognition', etc., user defined
    this.cover = null;
    this.title = null;
    this.message = '';  // The full content of this facebook post (text + parsed images)
    this.images = new Array();
    this.tags = new Array();
    this.remainingImages = 0;
    this.timestamp = 0;
    this.timestampUTC = '';
    this.contributor = '';
    this.reacted = new Object();
    this.commentCount = 0;
    this.textContent = '';
    this.imageContent = '';

    return this;
  };

  merge = $post => {
    this.origin = $post.origin || this.origin;
    this.translationOrigin = $post.translationOrigin || this.translationOrigin;
    this.translator = $post.translator || this.translator;
    this.groupId = $post.groupId || this.groupId;
    this.postId = $post.postId || this.postId;
    this.category = $post.category || this.category;
    this.collection = $post.collection || this.collection;
    this.cover = $post.cover || this.cover;
    this.title = $post.title || this.title;
    this.message = $post.message || this.message;
    this.images = [ ...this.images, ...$post.images ];
    this.tags = [ ...this.tags, ...$post.tags ];
    this.remainingImages = $post.remainingImages || this.remainingImages;
    this.timestamp = $post.timestamp || this.timestamp;
    this.timestampUTC = $post.timestampUTC || this.timestampUTC;
    this.contributor = $post.contributor || this.contributor;
    this.reacted = { ...this.reacted, ...$post.reacted };
    this.commentCount = $post.commentCount || this.commentCount;
    this.textContent = $post.textContent || this.textContent;
    this.imageContent = $post.imageContent || this.imageContent;

    return this;
  };

  finalize = () => {
    this.tags = this.tags.map(tag => tag.substring(1));
    this.message = [this.textContent, this.imageContent].join('\n\n\n\n');
    if (this.cover || this.title) {
      let cover = format(MT.IMAGE, '', this.cover);
      let title = this.title
        ?
          `${format(MT.H1, this.title)}\n\n`
        :
          ''
        ;
      this.message = `${cover}\n\n${title}${this.message}`;
    }

    if (this.origin && this.origin.includes('medium.com')) {
      this.category = 'medium';
    }

    return this;
  };

  serialize = () => ({
    origin: this.origin,
    translationOrigin: this.translationOrigin,
    translator: this.translator,
    groupId: this.groupId,
    postId: this.postId,
    format: 'md',
    category: this.category,
    collection: this.collection,
    cover: this.cover,
    title: this.title,
    message: this.message,
    images: this.images,
    tags: this.tags,
    remainingImages: this.remainingImages,
    timestamp: this.timestamp,
    timestampUTC: this.timestampUTC,
    contributor: this.contributor,
    reacted: this.reacted,
    commentCount: this.commentCount,
    textContent: this.textContent,
    imageContent: this.imageContent,
  });
};

class Fetcher {
  constructor() {
    this.fetchers = {
      cover: null,
      title: null,
      editor: null,
      timestamp: null,
      main: null,
      reactions: null,
      reactionDetails: null,
      commentCount: null,
      remainingImages: null,
      images: [],
    };

    this.post = new Post();
  }

  setFetcher = (name, selector, method) => {
    this.fetchers[name] = new Object();
    this.fetchers[name].selector = selector;
    this.fetchers[name].method = method;

    return this;
  };

  fetch = name => {
    const fetchee = this.fetchers[name];
    const post = this.post;
    if (!fetchee) {
      return null;
    }

    $(document).find(fetchee.selector).each(function() {
      let $node = $(this);
      if (!fetchee.out) {
        fetchee.out = fetchee.method($node, post);
      } else if (typeof fetchee.out === 'object' && !Array.isArray(fetchee.out)) {
        fetchee.out = Object.assign(fetchee.out, fetchee.method($node, post));
      } else if (Array.isArray(fetchee.out)) {
        fetchee.out = [ ...fetchee.out, ...fetchee.method($node, post) ];
      }
    });

    return this;
  };

  fetchAll = () => {
    for (let key in this.fetchers) {
      let fetchee = this.fetchers[key];
      let post = this.post;

      if (fetchee) {
        $(document).find(fetchee.selector).each(function() {
          let $node = $(this);
          if (!fetchee.out) {
            fetchee.out = fetchee.method($node, post);
          } else if (typeof fetchee.out === 'object' && !Array.isArray(fetchee.out)) {
            fetchee.out = Object.assign(fetchee.out, fetchee.method($node, post));
          } else if (Array.isArray(fetchee.out)) {
            fetchee.out = [ ...fetchee.out, ...fetchee.method($node, post) ];
          }
        });
      }
    }

    return this;
  };

  parseOrigin = payload => {
    let post = this.post;
    let groupAndPost = REGEX_GROUP_POST.exec(payload);
    let noteGroupTitlePost = REGEX_NOTE.exec(payload);
    if (payload.includes('groups') && payload.includes('permalink') && groupAndPost) {
      let groupId = groupAndPost[1];
      post.groupId = getIdFromName(groupId) || groupId;
      post.postId = +groupAndPost[2];
      post.category = getCategoryFromGroup(post.groupId);
    }

    if (payload.includes('notes') && noteGroupTitlePost) {
      let groupId = noteGroupTitlePost[1];
      post.groupId = getIdFromName(groupId) || groupId;
      post.postId = +noteGroupTitlePost[3];
      post.category = getCategoryFromGroup(post.groupId);
    }

    post.translationOrigin = payload;

    return this;
  };


  export = () => {
    let post = this.post;
    const {
      cover,
      title,
      editor,
      timestamp,
      main,
      reactions,
      reactionDetails,
      commentCount,
      remainingImages,
      images,
    } = this.fetchers;
    post.cover = cover ? cover.out : '';
    post.title = title ? title.out : '';
    post.translator = editor ? editor.out : '';
    post.timestamp = timestamp.out.timestamp;
    post.timestampUTC = timestamp.out.timestampUTC;
    post.textContent = main.out;
    post.reacted.total = reactions.out;
    if (reactionDetails) {
      post.reacted = { ...post.reacted.total, ...reactionDetails.out };
    }
    post.commentCount = commentCount.out;
    post.remainingImages = remainingImages ? remainingImages.out : 0;
    post.images = images.out ? images.out.images : [];
    post.imageContent = images.out ? images.out.imageContent : '';

    return post;
  };
};

let globalPost = new Post();
