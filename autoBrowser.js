const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');
const {
  getContentFromRelativePath,
  injectJQuery,
  createBrowser,
  closeBrowser,
  createNewPage,
  changePage,
  login,
} = require('./utils');


const {
  scrape,
  amendImages
} = require('./postFetcher.js');


const QRVN_ALIAS = 'vietnamquora';
const QRVN_GID = '1988191741413952';
const FACEBOOK = 'https://www.facebook.com';
const CONFIG = JSON.parse(getContentFromRelativePath('facebook/config.json'));
const CREDENTIALS = JSON.parse(getContentFromRelativePath('facebook/credentials.json'));
const POSTS = JSON.parse(getContentFromRelativePath('facebook/postIds.json'));
const OUTDIR = path.join(__dirname, 'output');
const MAIN_SCRIPT = getContentFromRelativePath('browserScripts/main.js');
const IMAGES_SCRIPT = getContentFromRelativePath('browserScripts/images.js');
const SINGLE_IMAGE_SCRIPT = getContentFromRelativePath('browserScripts/singleImage.js');


const generatePostUrl = (postId, groupId = QRVN_ALIAS) => {
  return `https://www.facebook.com/groups/${groupId}/permalink/${postId}`;
};


const generateImagesUrl = postId => {
  return `https://www.facebook.com/media/set/?set=pcb.${postId}`;
};


const fetchPage = async (browser, page, postId) => {
  const postUrl = generatePostUrl(postId);
  console.log(`Fetching page with url = ${postUrl}`);
  await changePage(page, postUrl);
  await injectJQuery(page);
  await page.evaluate(MAIN_SCRIPT);
  let postObj = await page.evaluate(scrape, postUrl);
  let detailedImages = [];
  if (postObj.remainingImages) {
    const imagesPage = await createNewPage(browser, generateImagesUrl(postId));
    const images = await imagesPage.evaluate(IMAGES_SCRIPT);
    for (let i = 0; i < images.length; i++) {
      const image = images[i];
      const singlePage = await createNewPage(browser, image.href);
      const detailedObj = await singlePage.evaluate(SINGLE_IMAGE_SCRIPT);
      await singlePage.close();
      detailedImages.push({
        ...image,
        ...detailedObj,
      });
    }
    await imagesPage.close();
  }
  postObj = await page.evaluate(amendImages, detailedImages);
  return postObj;
};


const extractId = post => {
  // const urlPattern = /https:\/\/www.facebook.com\/groups\/[a-zA-Z]+\/permalink\/([0-9]+)(\/)?/.exec(post);
  const urlPattern = /\/groups\/[a-zA-Z]+\/permalink\/([0-9]+)(\/)?/.exec(post);
  if (urlPattern && urlPattern.length) {
    return urlPattern[1];
  }
  return null;
};


const runSession = async () => {
  const browser = await createBrowser(CONFIG);
  const page = (await browser.pages())[0];
  await changePage(page, FACEBOOK);
  await login(page, CONFIG.selectors.auth, CREDENTIALS);
  for (let i = 0; i < POSTS.length; i++) {
    post = POSTS[i];
    console.log(`Attempt to handle ${JSON.stringify(post)}`);
    const postId = extractId(post);
    const content = await fetchPage(browser, page, postId);
    const filename = `${QRVN_GID}_${postId}.json`;
    fs.writeFileSync(path.join(OUTDIR, filename), JSON.stringify(content, null, 2));
  }
};


runSession()
  .then(
    () => {
      console.log("Success!");
    }
  )
  .catch(
    error => {
      console.log(`An error occurred: ${error}`);
      console.trace();
    }
  );
