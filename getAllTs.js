const puppeteer = require('puppeteer');
const fs = require('fs');

const {
  createBrowser,
  changePage,
  login,
} = require('./utils');


const CREDENTIALS = JSON.parse(fs.readFileSync('./facebook/credentials.json'));
const CONFIG = JSON.parse(fs.readFileSync('./facebook/config.json'));
const URL = CONFIG.url;
const SELECTORS = CONFIG.selectors;
const FACEBOOK = 'https://www.facebook.com';
const DELAY = 1000;


const sleep = dt => new Promise(
  (resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, dt)
  }
);


const viewByLatest = async (page, bodySels) => {
  await page.click(bodySels.seePost);
  // https://github.com/puppeteer/puppeteer/issues/3338
  // Use Promise.all if you don't want to create a promise variable
  // await Promise.all([page.click(bodySels.chronological), page.waitForNavigation()])
  page.click(bodySels.chronological);
  await page.waitForNavigation({ waitUntil: 'networkidle0' });
  // await page.waitFor(10000);
};


const getAllTimestamps = tsSels => {
  return Array.from(document.querySelectorAll(tsSels)).map(
    e => parseInt(e.getAttribute('data-utime'))
  );
};


const getMinTs = tss => tss[tss.length - 1];


const scrollToTimestamp = async (page, ts) => {
  while (true) {
    const prevHeight = await page.evaluate('document.body.scrollHeight');
    const tss = await page.evaluate(getAllTimestamps, SELECTORS.body.timestamp);
    if (getMinTs(tss) < ts) {
      break;
    }
    await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
    await page.waitForFunction(`document.body.scrollHeight > ${prevHeight}`);
  }
};


const extractItems = bodySels => {
  return Array.from(document.querySelectorAll(bodySels.singlePost)).map(
    element => {
      const link = element.querySelector(bodySels.permalink);
      return link ? link.getAttribute('href') : null;
    }
  ).filter(e => e);
};


const runSession = async () => {
  const browser = await createBrowser(CONFIG);
  const page = (await browser.pages())[0];
  await changePage(page, FACEBOOK);
  await login(page, CONFIG.selectors.auth, CREDENTIALS);
  // await changePage(page, URL);
  // await viewByLatest(page, CONFIG.selectors.body);
  await changePage(page, CONFIG.quoraChrono);
  await scrollToTimestamp(page, CONFIG.thresholdTs);
  const permalinks = await page.evaluate(extractItems, SELECTORS.body);
  fs.writeFileSync('./output.json', JSON.stringify(permalinks, null, 2));
  await browser.close();
};


runSession()
  .then(
    () => console.log("Success")
  )
  .catch(
    e => console.log(e)
  );
