const spanCaption = document.querySelector('.fbPhotosPhotoCaption#fbPhotoSnowliftCaption');
const caption = spanCaption.textContent;

const reactionDetails = '._6iid>span._1n9r._6iie>span._1n9k>a._1n9l';
const reactionCount = '._6iid>a._3dlf>span._3dlg>span._3dlh>span._81hb';

let reactionJson = { };
let detailedJson = { };

const allDetailedReactions = document.querySelectorAll(reactionDetails);
allDetailedReactions.forEach(
  reaction => {
    const valueAndKey = reaction.getAttribute('aria-label').split(' ');
    reactionJson[valueAndKey[1].toLowerCase()] = parseInt(valueAndKey[0]);
  }
);

const totalReaction = document.querySelector(reactionCount);
if (totalReaction) {
  reactionJson['total'] = parseInt(totalReaction.textContent);
}

detailedJson = {
  caption,
  reacted: reactionJson,
};

detailedJson
