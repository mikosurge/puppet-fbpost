let allPhotosJson = [];
const allPhotos = document.querySelectorAll('.fbPhotosRedesignBorderOverlay#fbTimelinePhotosContent');
allPhotos.forEach(
  photo => {
    const allCoverDivs = photo.querySelectorAll('._vor.focus_target._53s.fbPhotoCurationControlWrapper');
    const location = window.location.toString();
    const setPattern = /https:\/\/www.facebook.com\/media\/set\/\?set=pcb.([0-9]+)/.exec(window.location.toString());
    let setId = null;
    if (setPattern && setPattern.length) {
      setId = setPattern[1];
    }
    allCoverDivs.forEach(
      coverDiv => {
        const fbId = coverDiv.getAttribute('data-fbid');
        const innerImage = coverDiv.querySelector('img._pq3.img');
        // A cover div might have a photo or a video inside
        if (innerImage) {
          let href = '';
          if (setId) {
            href = `https://www.facebook.com/photo.php?fbid=${fbId}&set=pcb.${setId}`;
          } else {
            href = `https://www.facebook.com/photo.php?fbid=${fbId}`;
          }

          allPhotosJson.push({
            alt: innerImage.getAttribute('alt'),
            style: innerImage.getAttribute('style'),
            href
          });
        }
      }
    );
  }
);

allPhotosJson
