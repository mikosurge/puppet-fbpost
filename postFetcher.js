module.exports.scrape = url => {
  let fetcher = new Fetcher();
  fetcher.parseOrigin(url);
  if (url.includes('notes')) {
    fetcher
      .setFetcher('cover', FB_SEL[ET.NOTE_COVER], fetchNoteCover)
      .setFetcher('title', FB_SEL[ET.NOTE_TITLE], fetchNoteTitle)
      .setFetcher('editor', FB_SEL[ET.NOTE_EDITOR], fetchEditor)
      .setFetcher('timestamp', FB_SEL[ET.NOTE_TIMESTAMP], fetchNoteTimestamp)
      .setFetcher('main', FB_SEL[ET.NOTE_BODY], fetchMain)
      .setFetcher('reactions', FB_SEL[ET.NOTE_REACTION], fetchReactions)
      .setFetcher('commentCount', FB_SEL[ET.NOTE_COMMENT_COUNT], fetchCommentCount);
    fetcher.fetchAll();
  } else {
    fetcher
      .setFetcher('editor', FB_SEL[ET.EDITOR], fetchEditor)
      .setFetcher('timestamp', FB_SEL[ET.TIMESTAMP], fetchTimestamp)
      .setFetcher('main', FB_SEL[ET.POST], fetchMain)
      .setFetcher('reactions', FB_SEL[ET.REACTION], fetchReactions)
      .setFetcher('reactionDetails', FB_SEL[ET.REACTION_DETAILS], fetchReactionDetails)
      .setFetcher('commentCount', FB_SEL[ET.COMMENT_COUNT], fetchCommentCount)
      .setFetcher('remainingImages', FB_SEL[ET.MORE_IMAGES], fetchRemainingImages);

    fetcher.fetchAll();
    if (!fetcher.fetchers.remainingImages.out) {
      fetcher
        .setFetcher('images', FB_SEL[ET.IMAGE_CONTAINER], fetchImages)
        .fetch('images');
    }
  }

  globalPost.merge(fetcher.export());
  console.log(globalPost);
  return globalPost.serialize();
};


module.exports.amendImages = extractedImages => {
  if (extractedImages && extractedImages.length) {
    let postToAmend = new Post();
    let images = extractedImages
      .map(
        image => {
          let href = null;
          let urlPattern = REGEX_STYLE.exec(image.style);
          if (urlPattern.length && urlPattern[1]) {
            href = urlPattern[1].replace(REGEX_IMAGE_URL, `%$1`);
          }

          return {
            alt: image.alt,
            dataSrc: decodeURIComponent(href),
            href: image.href,
            caption: image.caption,
            reacted: image.reacted,
          };
        }
      )
      .filter(image => image.dataSrc);
    let markdownImages = images
      .map(
        image => format(MT.IMAGE, '', image)
      );

    if (!!images.length) {
      postToAmend.images = images;
    }
    postToAmend.imageContent = markdownImages.join('\n\n');
    globalPost.merge(postToAmend);
  }

  globalPost.finalize();
  return globalPost.serialize();
};
